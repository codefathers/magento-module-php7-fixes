<?php

/**
 * replacement for php7
 * Before PHP 7.1.0, if you call a function without passing required arguments,
 * a warning was generated, and the missing arguments defaulted to NULL.
 * In PHP 7.1.0, an ArgumentCountError is being thrown instead.
 *
 * Due its not possible to rewrite global methods, we have to change all
 * problematic calls of "call_user_func_array" "to call_user_func_array_i",
 * which suppresses notices, warnings and adds NULL-args
 *
 * call_user_func_array => call_user_func_array_i
 *
 * @param $callbk
 * @param $args
 * @return mixed
 */

if (!function_exists('call_user_func_array_i')) {
    function call_user_func_array_i($callbk,$args) {
        $old_value = error_reporting();
        error_reporting($old_value & ~E_NOTICE & ~E_WARNING );
        while(true) {
            try {
                return call_user_func_array($callbk,$args);
            } catch(ArgumentCountError $e) {
                $args[] = NULL;
            }
        }
        error_reporting($old_value);
    }
}
