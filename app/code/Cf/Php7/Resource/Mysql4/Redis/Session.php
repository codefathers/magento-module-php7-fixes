<?php
/**
 *
 * codefathers magento compatibility module for PHP7
 *
 * @category    Cf
 * @package     Cf_Php7
 * @copyright   Copyright (c) Achim Schweisgut, codefathers 2017
 */


/**
 * Class Cf_Php7_Resource_Mysql4_Redis_Session
 */
class Cf_Php7_Resource_Mysql4_Redis_Session extends Cm_RedisSession_Model_Session
{

    /**
     * Fetch session data
     *
     * @param string $sessId
     * @return string
     */
    public function read($sessId)
    {
        /**
         * php 7 fix: typecast on return value
         * (magento returns "false" on empty data sets)
         */
        return (string) parent::read($sessId);
    }


}
