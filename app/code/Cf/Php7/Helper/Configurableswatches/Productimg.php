<?php
/**
 *
 * codefathers magento compatibility module for PHP7
 *
 * @category    Cf
 * @package     Cf_Php7
 * @copyright   Copyright (c) Achim Schweisgut, codefathers 2017
 */

/**
 * Class Cf_Php7_Helper_Configurableswatches_Productimg
 */
class Cf_Php7_Helper_Configurableswatches_Productimg extends Mage_ConfigurableSwatches_Helper_Productimg
{

    /**
     * Determine whether to show an image in the product media gallery
     *
     * @param Mage_Catalog_Model_Product $product
     * @param Varien_Object $image
     * @return bool
     */
    public function filterImageInGallery($product, $image)
    {
        if (!Mage::helper('configurableswatches')->isEnabled()) {
            return true;
        }

        if (!isset($this->_productImageFilters[$product->getId()])) {
            /* bugfix for PHP7 */
            //$mapping = call_user_func_array("array_merge_recursive", $product->getChildAttributeLabelMapping());
            $mapping = call_user_func_array_i("array_merge_recursive", $product->getChildAttributeLabelMapping());
            $filters = array_unique($mapping['labels']);
            $filters = array_merge($filters, array_map(function ($label) {
                return $label . Mage_ConfigurableSwatches_Helper_Productimg::SWATCH_LABEL_SUFFIX;
            }, $filters));
            $this->_productImageFilters[$product->getId()] = $filters;
        }

        return !in_array(Mage_ConfigurableSwatches_Helper_Data::normalizeKey($image->getLabel()),
            $this->_productImageFilters[$product->getId()]);
    }
}
